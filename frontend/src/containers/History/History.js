import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {PageHeader, Panel} from "react-bootstrap";
import {getHistory} from "../../store/actions/artists";

class History extends Component {
    componentDidMount() {
        if (this.props.user) {
            this.props.getHistory(this.props.user._id);
        } else {
            this.props.history.push('login');
        }
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    История
                </PageHeader>
                {this.props.trackHistory.map(track => (
                    <Panel key={track.history._id}>
                        <Panel.Body>
                            {track.artist} - {track.history.trackID.name} {`(${track.history.trackID.duration})`} {track.history.dateTime}
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    trackHistory: state.artists.history,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getHistory: (id) => dispatch(getHistory(id))
});


export default connect(mapStateToProps, mapDispatchToProps)(History);