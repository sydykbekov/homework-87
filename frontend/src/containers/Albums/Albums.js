import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Image, PageHeader, Panel} from "react-bootstrap";
import {getAlbums} from "../../store/actions/artists";
import {Link} from "react-router-dom";

class Albums extends Component {
    componentDidMount() {
        this.props.getAlbums(this.props.match.params.id);
    }
    render() {
        return (
            <Fragment>
                <PageHeader>
                    Альбомы
                </PageHeader>
                {this.props.albums.map(album => (
                    <Panel key={album._id}>
                        <Panel.Body>
                            <Image
                                style={{width: '100px', marginRight: '10px'}}
                                src={'http://localhost:8000/uploads/' + album.image}
                                thumbnail
                            />
                            <Link to={this.props.history.location.pathname + '/album/' + album._id}>
                                {album.name} {`(${album.year})`}
                            </Link>
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    albums: state.artists.albums
});

const mapDispatchToProps = dispatch => ({
    getAlbums: id => dispatch(getAlbums(id))
});


export default connect(mapStateToProps, mapDispatchToProps)(Albums);