import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {PageHeader, Panel} from "react-bootstrap";
import {getTracks, sendHistory} from "../../store/actions/artists";

class Tracks extends Component {
    componentDidMount() {
        this.props.getTracks(this.props.match.params.albumID);
    }

    send = (trackID) => {
        if (this.props.user) {
            const info = {
                userID: this.props.user._id,
                trackID: trackID
            };

            this.props.sendHistory(info, this.props.user.token);
        }
    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Треки
                </PageHeader>
                {this.props.tracks.map(track => (
                    <Panel key={track.track._id}>
                        <Panel.Body>
                            <p className="track"
                               onClick={() => this.send(track.track._id)}>{track.number}. {track.track.name} {`(${track.track.duration})`}</p>
                        </Panel.Body>
                    </Panel>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    tracks: state.artists.tracks,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getTracks: id => dispatch(getTracks(id)),
    sendHistory: (listenedTrack, token) => dispatch(sendHistory(listenedTrack, token))
});


export default connect(mapStateToProps, mapDispatchToProps)(Tracks);